# Access Permission Change
This is a service which can change access permission for a lot of your Internal Services

## Getting Started
These instruction will get you a copy of project and help to run up it for developing purposes.

## Prerequisites
For good practice and working w/out any problem you need to install next software:

> Needed service 

## Installing 
For getting service and running up on your local machine please follow next steps: 
```
1. Run COMMAND “brew install apc”
2. Wait while process will be finished
3. Run COMMAND “apc run --configuration”
4. Answer question
```


## Running the test

For running test of APC please config it on 1 of your test machine and run

	apc run --test1

## Configuration

If need config for prod run command

	apc run  --configuration

For running 

	apc run --full


## Authors

* Aleksandr Golovchenko

## License

This project is under GPT License
